<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query = "select * from rental_booking INNER JOIN user ON rental_booking.user_id=user.user_id INNER JOIN car_type ON rental_booking.car_type_id=car_type.car_type_id where rental_booking.rental_booking_id='".$_GET['id']."'";
$result = $db->query($query);
$ride=$result->row;
$driver_id = $ride['driver_id'];
if($driver_id == 0)
{
    $driver_name = "Not Assgin";
    $driver_image = "Not Assgin";
    $driver_email = "Not Assgin";
    $driver_phone = "Not Assgin";
}else{
    $query1 = "select * from driver where driver_id ='$driver_id'";
    $result1 = $db->query($query1);
    $list1 = $result1->row;
    $driver_name = $list1['driver_name'];
    $driver_image = $list1['driver_image'];
    $driver_email = $list1['driver_email'];
    $driver_phone = $list1['driver_phone'];
}
$query = "select * from table_done_rental_booking where rental_booking_id='".$_GET['id']."'";
$result = $db->query($query);
$done_ride = $result->row;
if(!empty($done_ride)){
$drop_lat = $done_ride['end_lat'];
if (empty($drop_lat)) {
  $drop_lat = $ride['pickup_lat'];
}
$drop_long = $ride['end_long'];
if(empty($drop_long))
{
    $drop_long = $ride['pickup_long'];
}
$drop_location = $ride['end_location'];
if(empty($drop_location))
{
    $drop_location = $ride['pickup_location'];
}
$total_distance_travel = $done_ride['total_distance_travel'];
$total_time_travel = $done_ride['total_time_travel'];
$rental_package_price = $done_ride['rental_package_price'];
$final_bill_amount = $done_ride['final_bill_amount'];
}else{
$drop_lat = $ride['pickup_lat'];
$drop_long = $ride['pickup_long'];
$drop_location = $ride['pickup_location'];
$total_distance_travel = "Ride Not Completed";
$total_time_travel = "Ride Not Completed";
$rental_package_price = "Ride Not Completed";
$final_bill_amount = "Ride Not Completed";
}
$lat = $ride['pickup_lat'];
$pickup_long = $ride['pickup_long'];
$pickup_location = $ride['pickup_location'];
?>
<style>
    .divmap{ height: 500px}
</style>

<div class="wraper container-fluid">
    <div class="row">

        <div id="dvMap" class="divmap col-md-4">
            <iframe width="100%" height="700" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q=<?php echo $pickup_location; ?>&output=embed"></iframe>
        </div>
        <div class="col-md-8">

            <table class="table table-striped table-hover table-bordered" id="">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td><strong>Rider Name</strong></td><td><?php
                                       echo $ride['user_name'];
                                        ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Rider Email</strong></td><td><?php
                                        $user_email = $ride['user_email'];
                                        echo $user_email;
                                        ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Rider Phone</strong></td><td><?php
                                        $user_phone=$ride['user_phone'];
                                        echo $user_phone;
                                        ?></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td><strong>Driver Name</strong></td><td><?= $ride['driver_name'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Driver Email</strong></td><td><?php
                                        $driver_email = $ride['driver_email'];
                                        echo $driver_email;
                                    ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Driver Phone</strong></td><td><?php
                                        $driver_phone = $ride['driver_phone'];
                                        echo $driver_phone;
                                        ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>


</table>
            <table class="table table-striped table-hover table-bordered" id="">
                <tbody>
            <tr>
                <td width="150"><strong>Pickup Up Location</strong></td>
                <td><?= $ride['pickup_location'];?></td>
            </tr>
            <tr>
                <td><strong>Drop Up Location</strong></td>
                <td><?= $drop_location;?></td>
            </tr>
            <tr>
                <td><strong>Car type</strong></td>
                <td><?= $ride['car_type_name'];?></td>
            </tr>
            <tr>
                <td><strong>Total Distance Travel </strong></td>
                <td><?= $total_distance_travel;?></td>
            </tr>
            <tr>
                <td><strong>Total Time Travel </strong></td>
                <td><?= $total_time_travel;?></td>
            </tr>
            <tr>
                <td><strong>Final Bill Amount </strong></td>
                <td><?= $final_bill_amount;?></td>
            </tr>
            </tbody>
            </table>

            <table class="table table-striped table-hover table-bordered" id="">
                <tbody>
                <tr>
                    <th width="150"><strong>Vehicle Start Meter Readings</strong></th>
                    <th width="150"><strong>Vehicle End Meter Readings</strong></th>
                </tr>
                <tr>
                    <td><?php $start_ride =  $ride['start_meter_reading'];
                    if($start_ride == 0){
                        echo "Not Start";
                    }else{
                        echo $start_ride;
                    }
                    ?></td>
                    <td><?php $end_meter_reading =  $ride['end_meter_reading'];
                        if($end_meter_reading == 0){
                            echo "Not End";
                        }else{
                            echo $end_meter_reading;
                        }
                        ?></td>
                </tr>
                <tr>
                    <th colspan="3" style="text-align: center;">Images</th>
                </tr>
                <tr>
                    <?php $filenotexit = "http://apporio.co.uk/apporiotaxi/uploads/driver/filenotexit.png"; ?>
                    <?php $fileexit = "http://apporio.co.uk/apporiotaxi/uploads/driver/fileexit.png"; ?>
                    <td>
                    <?php $start_meter_reading_image =  $ride['start_meter_reading_image'];
                    if(!empty($start_meter_reading_image)){ ?>
                        <a href="<?php echo $start_meter_reading_image;?>" target="_blank"><img src="<?php echo $fileexit; ?>" width="150px" height="150px"></a>
                        <?php
                    }else{ ?>
                        <img src="<?php echo $filenotexit; ?>" width="150px" height="150px">
                <?php  }
                    ?>
                    </td>
                    <td>
                        <?php $end_meter_reading_image =  $ride['end_meter_reading_image'];
                        if(!empty($end_meter_reading_image)){ ?>
                            <a href="<?php echo $end_meter_reading_image;?>" target="_blank"><img src="<?php echo $fileexit; ?>" width="150px" height="150px"></a>
                            <?php
                        }else{ ?>
                            <img src="<?php echo $filenotexit; ?>" width="150px" height="150px">
                        <?php  }
                        ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

