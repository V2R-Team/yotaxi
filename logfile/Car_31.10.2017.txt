Rental - Car Type Api: October 31, 2017, 4:19 am
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => HATCHBACK
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [base_fare] => 80 Per 3 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 121
            [base_fare] => 100 Per 5 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => Mini
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Boca del Río
latitude: 19.155515196882586
longitude: -96.11133947968484
-------------------------
