<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header('Content-Type: application/json; Charset=UTF-8');

//$language_id=$_REQUEST['language_id'];
$language_id = 1;

	$query = "select * from city INNER JOIN price_card ON city.city_id=price_card.city_id where city.city_admin_status=1";
	$result = $db->query($query);
	$city=$result->rows;
	if(!empty($city))
	{
		$cities = array();
        foreach($city as $k => $v)
        {
            $cities[$v['city_id']]['city_id']=$v['city_id'];
            $cities[$v['city_id']]['city_name']=$v['city_name'];
            $cities[$v['city_id']]['city_latitude'] = $v['city_latitude'];
            $cities[$v['city_id']]['city_longitude'] = $v['city_longitude'];
            $cities[$v['city_id']]['city_admin_status'] = $v['city_admin_status'];
			$cities[$v['city_id']]['currency'] = $v['currency'];
			$cities[$v['city_id']]['distance'] = $v['distance'];
			$cities[$v['city_id']]['city_name_arabic'] = $v['city_name_arabic'];
			$cities[$v['city_id']]['city_name_french'] = $v['city_name_french'];
			$cities[$v['city_id']]['city_admin_status'] = $v['city_admin_status'];
        }
        $city = array();
        foreach($cities as $v){
            $city[] = array(
			                'city_id'=>$v['city_id'],
							'city_name'=>$v['city_name'],
							'city_latitude'=>$v['city_latitude'],
							'city_longitude'=>$v['city_longitude'],
							'city_admin_status'=>$v['city_admin_status'],
							 'currency'=> $v['currency'],
                             'distance'=> $v['distance'],
							 'city_name_arabic'=> $v['city_name_arabic'],
								'city_name_french'=> $v['city_name_french']
							);
        }
		
			$re = array('result'=> 1,'msg'	=> $city);
	}
	else
	{
		 $language="select * from messages where language_id='$language_id' and message_id=6";
         $lang_result = $db->query($language);
         $lang_list=$lang_result->row;
          $message_name=$lang_list['message_name'];
	  $re = array('result' => 0,'msg'	=>$message_name);
	}

echo json_encode($re, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
?>